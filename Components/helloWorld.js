import {Component} from "./component.js";
import {React} from "../Core/React.js";

export class HelloWorld extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        type: "object",
        properties: {
            name: {type: "string", enum: ["world", "you", "me", "Karl"]},
            me: {type: "string"},
            id: {type: "number"}
        }
    };

    render() {
        return React.createElement("div", {
            toWhat: {
                name: this.props.name,
                me: this.props.me
            },
            id: this.props.id,
            class: "castex"
        }, [
            "Hello {{toWhat.name}} from {{toWhat.me}}"
        ]);
    }
}