import {Component} from "./component.js";
import {React} from "../Core/React.js";
import {NavPrincipale} from "./navPrincipale.js";
import {Nav} from "./nav.js";
import {Table} from "./table.js";
import {uniqueId} from "../Helpers/uniqueId.js";
import {Footer} from "./footer.js";


export class PageUsersX extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        type: "object",
        properties: {
            id: {type: "number"}
        }
    };

    render() {
        return React.createElement("div", {id: this.props.id}, [
            React.createElement(NavPrincipale, {id: uniqueId()}),
            React.createElement("div", {id: this.props.id, class: "row align-items-start"}, [
                React.createElement(Nav, {
                    navItem: {Verticale: "users", Horizontale: "users-X"},
                    id: uniqueId(),
                    class: "nav bg-light col-2 flex-column"
                }),
                React.createElement("div", {id: this.props.id, class: "col-10"}, [
                    React.createElement(Table, {
                        label: ["Prenom", "Nom", "Fonction"],
                        data: [{
                            Nom: "Macron",
                            Prenom: "Emmanuel",
                            Fonction: "Président"
                        }, {
                            Nom: "Castex",
                            Prenom: "Jean",
                            Fonction: "Premier ministre"
                        }, {
                            Nom: "Véran",
                            Prenom: "Olivier",
                            Fonction: "Ministre de la santé"
                        }],
                        id: uniqueId(),
                        direction: "x"
                    })
                ])
            ]),
            React.createElement(Footer, {id: uniqueId()}, [])
        ]);
    }
}