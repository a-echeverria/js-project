import {Component} from "./component.js";
import {React} from "../Core/React.js";
import {Nav} from "./nav.js";
import {uniqueId} from "../Helpers/uniqueId.js";

export class NavPrincipale extends Component {
    static propTypes = {
        type: "object",
        properties: {
            id: {type: "number"}
        }
    };

    constructor(props) {
        super(props);
    }

    render() {
        return React.createElement(Nav, {
            navItem: {
                Accueil: "js-project",
                Multiplication: "multiplication",
                Gouvernement: "users"
            }, id: uniqueId(), class: 'nav justify-content-center bg-dark'
        })
    }
}