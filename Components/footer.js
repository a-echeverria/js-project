import {Component} from "./component.js";
import {React} from "../Core/React.js";

export class Footer extends Component {
    static propTypes = {
        type: "object",
        properties: {
            id: {type: "number"}
        }
    };

    render() {
        return React.createElement("div", {id: this.props.id}, [
            React.createElement("footer", {class: "footer"}, ["© ESGI"])
        ]);
    }
}
