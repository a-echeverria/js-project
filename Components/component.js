export class Component {
    constructor(props) {
        // this.props = props;
    }

    oldProps = {};
    props = {};

    prevRender = undefined;

    prevState = {};
    state = {};

    setState(state) {
        this.prevState = { ...this.state };
        this.state = {...this.prevState, ...state};
        this.display(this.props);
    };

    display(props) {
        this.receiveProps(props);
        if (this.shouldUpdate()) this.prevRender = this.render();
        return this.prevRender;
    }

    shouldUpdate = () => JSON.stringify(this.oldProps) !== JSON.stringify(this.props) ||
        JSON.stringify(this.prevState) !== JSON.stringify(this.state);

    receiveProps(props) {
        this.oldProps = {...this.props};
        this.props = props;
    }

    render() {
    }
}
