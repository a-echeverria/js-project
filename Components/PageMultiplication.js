import {Component} from "./component.js";
import {React} from "../Core/React.js";
import {NavPrincipale} from "./navPrincipale.js";
import {Counter} from "./counter.js";
import {uniqueId} from "../Helpers/uniqueId.js";
import {Footer} from "./footer.js";

export class PageMultiplication extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        type: "object",
        properties: {
            id: {type: "number"}
        }
    };

    mutiplicationTable() {
        const children = [];
        for (let i = 1; i < 11; i++) {
            children.push(
                React.createElement(Counter, {counter: 0, label: "Table de " + i, increment: i, id: "table" + i}))
        }
        return children;
    }

    render() {
        return React.createElement("div", {id: this.props.id}, [
            React.createElement(NavPrincipale, {id: uniqueId()}),
            React.createElement("div", {class: "row d-flex justify-content-center"}, this.mutiplicationTable()),
            React.createElement(Footer, {id: uniqueId()}, [])
        ]);
    }
}