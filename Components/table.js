import {Component} from "./component.js";
import {React} from "../Core/React.js";

export class Table extends Component {

    static propTypes = {
        label: {type: "array"},
        data: {type: "array"},
        id: {type: "number"},
        direction: {type: "string", enum: ["x", "y"]}
    };

    constructor(props) {
        super(props);
    }

    fillTable() {
        const children = [];
        if (this.props.direction === "y") {
            const data = this.fillTd(this.props.label);
            children.push(
                this.fillTh()
            );

            for (let i = 0; i < data.length; i++) {
                children.push(this.fillTr(data[i]));
            }
        } else if (this.props.direction === "x") {
            const data = this.fillRow();
            for (let i = 0; i < data.length; i++) {
                children.push(data[i]);
            }
        }

        return children;
    }

    fillTr(data) {
        return React.createElement("tr", {}, data);
    }

    fillRow() {
        const children = [];
        for (let i = 0; i < this.props.label.length; i++) {
            const row = [];
            const data = this.fillTd([this.props.label[i]]);
            row.push(
                React.createElement("th", {label: this.props.label[i]}, [
                    "{{label}}"
                ])
            )
            for (let j = 0; j < data.length; j++) {
                Array.isArray(data[j]) ? row.push(...data[j]) :
                    row.push(data[j]);
            }

            children.push(this.fillTr(row))
        }
        return children;
    }


    fillTh() {
        const children = [];
        for (let i = 0; i < this.props.label.length; i++) {
            children.push(
                React.createElement("th", {label: this.props.label[i]}, [
                    "{{label}}"
                ])
            )
        }
        return this.fillTr(children);
    }

    fillTd(attribut) {
        const row2 = [];
        for (let i = 0; i < this.props.data.length; i++) {
            const item = [];
            for (let element of attribut) {
                item.push(
                    React.createElement("td", {data: this.props.data[i][element]}, [
                        "{{data}}"
                    ])
                )

            }
            row2.push(item);

        }
        return row2;
    }

    render() {
        return React.createElement("table", {id: this.props.id}, this.fillTable());
    }

}