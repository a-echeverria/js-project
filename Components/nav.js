import {Component} from "./component.js";
import {React} from "../Core/React.js";
import {uniqueId} from "../Helpers/uniqueId.js";

export class Nav extends Component {
    static propTypes = {
        type: "object",
        properties: {
            navItem: {type: "object"},
            class: {type: "string"},
            id: {type: "number"}
        }
    };

    constructor(props) {
        super(props);
    }

    fillNavItem() {
        const children = [];
        for (const element in this.props.navItem) {
            if (element === "prop_access") continue;
            children.push(
                React.createElement("a", {
                    label: element,
                    href: this.props.navItem[element],
                    class: "link nav-link nav-item"
                }, [
                    "{{label}}"
                ])
            )
        }
        return children;
    }

    render() {
        return React.createElement("nav", {id: uniqueId(), class: this.props.class}, this.fillNavItem());
    }
}