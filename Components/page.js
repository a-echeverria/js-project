import {Component} from "./component.js";
import {React} from "../Core/React.js";
import {NavPrincipale} from "./navPrincipale.js";
import {HelloWorld} from "./helloWorld.js";
import {uniqueId} from "../Helpers/uniqueId.js";
import {Footer} from "./footer.js";

export class Page extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        type: "object",
        properties: {
            id: {type: "number"}
        }
    };

    render() {
        return React.createElement("div", {id: this.props.id}, [
            React.createElement(NavPrincipale, {id: uniqueId()}),
            React.createElement(HelloWorld, {name: "Karl", me: "Castex", id: uniqueId()}),
            React.createElement(Footer, {id: uniqueId()}, [])
        ]);
    }
}