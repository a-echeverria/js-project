import {Component} from "./component.js";
import {React} from "../Core/React.js";

export class Counter extends Component {
    static propTypes = {
        type: "object",
        properties: {
            counter: {type: "number"},
            label: {type: "string"},
            increment: {type: "number"},
            id: {type: "string"}
        }
    };

    constructor(props) {
        super(props);
        this.state.counter = props.counter;
    }

    state = {
        counter: undefined,
        clickNumber: 0
    }

    render() {
        return React.createElement("div", {id: this.props.id, class: "card col-4"}, [
            React.createElement(
                "button",
                {
                    onClick: () => this.setState({
                        counter: this.state.counter + this.props.increment,
                        clickNumber: this.state.clickNumber + 1
                    }),
                    label: this.props.label
                },
                ["{{label}}"]
            ),
            React.createElement("span", {
                class: "col-4",
                title: this.state.counter,
                clickNumber: this.state.clickNumber,
                increment: this.props.increment
            }, [
                "{{clickNumber}} X {{increment}} = {{title}}"
            ]),
        ]);
    }
}
