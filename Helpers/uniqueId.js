import {Cache} from "../Core/Cache.js";

export function uniqueId() {
    let id = Math.floor(Math.random() * 999999);
    if (Cache.get(id)) id = uniqueId();

    return id;
}