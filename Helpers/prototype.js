Object.prototype.prop_access = function (path) {
    if (this == null) return path + " object does not exist";
    if (typeof (path) != "string" || path === "") return this;
    let pathSplitted = path.split(".");
    let obj = this;
    for (let elem of pathSplitted) {
        if (typeof (obj[elem]) == "undefined") {
            return path + " property does not exist";
        }
        obj = obj[elem];
    }

    return obj;
};

String.prototype.interpolate = function (props) {
    const start = this.search("{{");
    const end = this.search("}}");
    if (typeof start === "number" && typeof end === "number"
        && start > -1 && end > -1) {
        const path = this.slice(start + 2, end).trim();
        const toReplace = this.slice(start, end + 2);

        return this.replace(toReplace, props.prop_access(path)).interpolate(props);
    }else {
        return this;
    }
};

