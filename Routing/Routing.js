import {Page} from "../Components/page.js"
import {PageMultiplication} from "../Components/PageMultiplication.js"
import {PageUsers} from "../Components/PageUsers.js"
import {PageUsersX} from "../Components/PageUsersX.js"

export default class Routing {
    routes = [
        {
            "component": Page,
            "path": "js-project",
        }, {
            "component": PageMultiplication,
            "path": "multiplication",
        }, {
            "component": PageUsers,
            "path": "users",
        },
        {
            "component": PageUsersX,
            "path": "users-X",
        },
    ]
}