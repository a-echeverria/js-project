import {type_check} from "../Helpers/type_check.js";
import "../Helpers/prototype.js";
import {Cache} from "./Cache.js";

export let ReactDOM = {
    render(rElement, hElement) {
        hElement.appendChild(rElement);
    },
};

export let React = {
    createElement(tagOrComponent, props, children) {
        let element;
        if (typeof tagOrComponent === "string") {
            element = document.createElement(tagOrComponent);
            for (let attribute in props) {
                // prop_access est considéré comme une propriété alors qu'il est défini en tant que prototype,
                // pour un soucis de lisibilité, on skip si l'attribut = prop_access
                if (attribute === "prop_access") continue;
                const regex = new RegExp('^on[A-Z]');
                regex.test(attribute.toString()) ?
                    element.addEventListener(attribute.slice(2).toLowerCase(), props[attribute]) :
                    element.setAttribute(attribute, props[attribute]);
            }
            for (let subElement of children) {
                if (typeof subElement === "string")
                    subElement = document.createTextNode(
                        subElement.interpolate(props)
                    );
                element.appendChild(subElement);
            }
            if (props.id && document.getElementById(`${props.id}`)) {
                const oldElement = document.getElementById(`${props.id}`);
                oldElement.replaceWith(element)
            }
        } else {
            if (!type_check(props, tagOrComponent.propTypes)) {
                throw new TypeError();
            }

            if (!Cache.get(props.id)) {
                Cache.add(props.id, new tagOrComponent(props));
            }
            return (Cache.get(props.id)).display(props)
        }

        return element;
    },
};

