export let Cache = {
    cache: {},
    add(id, component) {
        this.cache[`${id}`] = component;
    },
    get(id) {
        return this.cache[`${id}`];
    },
    clear() {
        this.cache = {};
    }
}