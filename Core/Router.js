import {React, ReactDOM} from "./React.js";
import Routing from "../Routing/Routing.js";
import {uniqueId} from "../Helpers/uniqueId.js";
import {Cache} from "./Cache.js";

export default class Router {
    routes = [];
    rootUrl = "/";
    current = "";

    constructor() {
        this.loadRoutes();
        this.listenChange();
    }

    addRoute = (path, component) => {
        this.routes.push({path, component});
        return this;
    };

    clearSlashes = (path) => path.toString().replace(/\/$/, "").replace(/^\//, "");

    listenChange = () => {
        setInterval(this.interval, 200);
        clearInterval(this.interval);
    }

    interval = () => {

        if (this.current === this.getFragment()) return;
        this.current = this.getFragment();
        let component = null;
        const hasMatch = this.routes.some(route => {
            const match = this.current === route.path;
            if (match) {
                component = route.component;
            }
            return match;
        });

        if (!hasMatch){
            console.error("Route n'existe pas");
            return;
        }

        this.resetView();

        ReactDOM.render(React.createElement(component, {id: uniqueId()}), document.getElementById("root"));

        const links = document.querySelectorAll("a.link")
        links.forEach(link => link.addEventListener('click', (e) => {
            e.preventDefault()
            this.goTo(e.currentTarget.getAttribute("href"))
        }))
    };

    resetView = () =>
    {
        const root = document.getElementById("root");
        for (let i = 0; i < root.childNodes.length; i++)
            root.removeChild(root.childNodes[i]);
        Cache.clear();
    }

    goTo = (path = "") => {
        window.history.pushState(null, null, this.rootUrl + this.clearSlashes(path));
        return this;
    }

    getFragment = () => {
        let fragment;
        fragment = this.clearSlashes(decodeURI(window.location.pathname + window.location.search));
        fragment = fragment.replace(/\?(.*)$/, "");
        fragment = this.rootUrl !== "/" ? fragment.replace(this.rootUrl, "") : fragment;
        return this.clearSlashes(fragment);
    };

    loadRoutes() {
        const routing = new Routing()
        routing.routes.forEach(route => this.addRoute(route.path, route.component));
    }
}